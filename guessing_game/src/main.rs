extern crate  rand;

use std::io; // io library from standart library
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    println!("Guess a number!");

    let secret_number = rand::thread_rng().gen_range(1,101);

    //println!("The secret number is: {}", secret_number);

    loop {
        println!("Please input your guess.");

        let mut guess = String::new(); //new string probably allocated;
        /*
        variable binding
        all variable are immutable by default. Except guess at the top cause mut
        String - growable utf-8 encoded bit of text
        :: some kind of static method - 'associated function' of particular type
        It creates a new empty string
        */
        io::stdin().read_line(&mut guess) // reading input;
        /*
        io is some kind of namespace here. ::stdin - using associated function
        stdin returns a handle to the standart input for terminal
        .read_line() - calling the method on our handle
        &mut guess - mutable reference(probably C# like). References are immutable by def
        */
        .expect("Failed to read line"); // failing if something wrong

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue, // next loop iteration
        }; // shadowing guess variable + handling error

        println!("You guessed: {}", guess); // printing to console input

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
